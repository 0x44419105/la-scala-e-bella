import scala.concurrent.ExecutionContext

import cats.effect.{ExitCode, IO, IOApp}
import cats.implicits._
import com.typesafe.config.ConfigFactory
import fs2.Stream
import fs2.concurrent.SignallingRef
import fs2.kafka._
import io.chrisdavenport.log4cats.Logger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import io.circe.config.parser
import io.circe.generic.auto._
import io.circe.parser.parse
import io.circe.syntax._
import io.circe.{Decoder, Encoder}

object App extends IOApp {

  implicit val logger: Logger[IO] = Slf4jLogger.getLogger[IO]
  implicit val ec: ExecutionContext = executionContext

  override def run(args: List[String]): IO[ExitCode] =
    for {
      httpConfig  <- config[HttpConfig]("http")
      cSetting    <- getConsumerSettings
      pSetting    <- getProducerSettings
      client      <- HttpClient.make(httpConfig)
      token       <- client.getJWT(httpConfig.credentials)
      tournament   = httpConfig.tournament
      _           <- client.subscribeToTournament(tournament, token)
      _           <- gameGo(client)(cSetting, pSetting).compile.drain
    } yield ExitCode.Success

  def config[T: Decoder](path: String): IO[T] =
    for {
      appConfig  <- IO(ConfigFactory.load("application"))
      pathConfig <- parser.decodePathF[IO, T](appConfig, path)
    } yield pathConfig

  def proposeInit(gs: GameStart): BattleshipPosition =
    Canvas.proposeInit(gs)

  def gameGo(http: HttpClient)(cSetting: ConsumerSettings[IO, Option[String], String], pSetting: ProducerSettings[IO, Option[String], String]) =
    for {
      start          <- kafkaReadStream[GameStart]("cc.battleships.game.started", cSetting)
      position        = proposeInit(start)
      _              <- Stream.eval(http.placeBattleship(position.x, position.y, position.direction, start.gameId))
      canvas          = Canvas(start.battlegroundSize, start.battlegroundSize).init(start.battleshipTemplate, position)
      finishedRounds <- Stream.eval(roundAndFinishGo(start.gameId, canvas)(cSetting, pSetting))
    } yield finishedRounds

  def roundAndFinishGo(gameId: String, canvas: Canvas)(cSetting: ConsumerSettings[IO, Option[String], String], pSetting: ProducerSettings[IO, Option[String], String]) =
    SignallingRef[IO, Boolean](false).flatMap { signal =>
      val s1 = roundGo(canvas)(cSetting, pSetting).interruptWhen(signal)
      val s2 = finishGo(gameId, signal)(cSetting, pSetting)

      s1.concurrently(s2).compile.toVector
    }

  def finishGo(gameId: String, signal: SignallingRef[IO, Boolean])(cSetting: ConsumerSettings[IO, Option[String], String], pSetting: ProducerSettings[IO, Option[String], String]) = {
    kafkaReadStream[GameEnd]("cc.battleships.game.ended", cSetting)
      .filter(_.gameId === gameId)
      .map { end =>
        signal.set(true)
        end
      }
  }

  def roundGo(c: Canvas)(cSetting: ConsumerSettings[IO, Option[String], String], pSetting: ProducerSettings[IO, Option[String], String]) =
    for {
      start         <- kafkaReadStream[RoundStart]("cc.battleships.round.started", cSetting)
      shoot          = Stream.emit(c.proposeShot).map(ShootEvent.fromRoundStart(start, _))
      _             <- kafkaWriteStream[ShootEvent](shoot, "cc.battleships.shot", pSetting)
      end           <- kafkaReadStream[RoundEnd]("cc.battleships.round.ended", cSetting)
      _              = c.mark(end)
    } yield end

  def getConsumerSettings: IO[ConsumerSettings[IO, Option[String], String]] =
    for {
      bootStrapServers <- App.config[BootstrapServers]("kafka.bootstrapServers")
      groupId          <- App.config[GroupId]("kafka.groupId")
    } yield
      ConsumerSettings[IO, Option[String], String]
        .withAutoOffsetReset(AutoOffsetReset.Earliest)
        .withBootstrapServers(bootStrapServers.serverSetting)
        .withGroupId(groupId.id)
        .withProperties(
          "security.protocol" -> "SASL_SSL",
          "sasl.mechanism" -> "PLAIN",
          "sasl.jaas.config" -> "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"DOYHCXGJGGBSBBSZ\" password=\"QQERbnI4wF5B0CslS6Bcg1zPuLsIC9yvUhm7MWnIeX1w+YJwfBDaAEs5x7AxpM+b\";"
        )

  def getProducerSettings: IO[ProducerSettings[IO, Option[String], String]] =
    for {
      bootStrapServers <- App.config[BootstrapServers]("kafka.bootstrapServers")
    } yield {
      ProducerSettings[IO, Option[String], String]
        .withBootstrapServers(bootStrapServers.serverSetting)
        .withProperties(
          "security.protocol" -> "SASL_SSL",
          "sasl.mechanism" -> "PLAIN",
          "sasl.jaas.config" -> "org.apache.kafka.common.security.plain.PlainLoginModule required username=\"DOYHCXGJGGBSBBSZ\" password=\"QQERbnI4wF5B0CslS6Bcg1zPuLsIC9yvUhm7MWnIeX1w+YJwfBDaAEs5x7AxpM+b\";"
        )
    }

  def kafkaReadStream[T](topicName: String, consumerSettings: ConsumerSettings[IO, Option[String], String])(implicit decoder: Decoder[T]) = {
    KafkaConsumer.stream(consumerSettings)
      .evalTap(_.subscribeTo(topicName))
      .flatMap(_.stream)
      .map(e => parse(e.record.value).flatMap(_.as[T]))
      .flatMap(attempt => Stream.eval(IO.fromEither(attempt)))
  }

  def kafkaWriteStream[T](streamToWrite: Stream[IO, T], topicName: String, producerSettings: ProducerSettings[IO, Option[String], String])(implicit encoder: Encoder[T]) = {
    KafkaProducer.stream(producerSettings)
      .flatMap { producer =>
        streamToWrite
          .map { elem => ProducerRecords.one(ProducerRecord(topicName, None, elem.asJson.toString))}
          .evalMap { value => producer.produce(value).flatten }
      }
  }
}
