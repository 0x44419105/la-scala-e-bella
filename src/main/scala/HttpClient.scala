import scala.concurrent.ExecutionContext

import HttpClient._
import cats.effect.{IO, _}
import io.chrisdavenport.log4cats.Logger
import io.circe.generic.auto._
import org.http4s
import org.http4s._
import org.http4s.circe._
import org.http4s.client.Client
import org.http4s.client.blaze.BlazeClientBuilder
import org.http4s.client.dsl.io._
import org.http4s.dsl.io._
import org.http4s.headers.Authorization

class HttpClient(httpConfig: HttpConfig, client: Resource[IO, Client[IO]]) {

  def getJWT(credentials: Credentials): IO[String] =
    for {
      uri      <- IO.fromEither(Uri.fromString(httpConfig.api + "/api/authenticate"))
      request  <- POST(credentials, uri)
      response <- client.use(res => res.expect[JWT](request))
    } yield response.token

  //api/tournaments/BnIy16hjAt/teams
  def subscribeToTournament(gameId: String, jwt: String): IO[Status] =
    for {
      uri      <- IO.fromEither(Uri.fromString(httpConfig.api + "/api/tournaments" + s"/$gameId" + "/teams"))
      headers  <- getAuthHeaders(jwt)
      request  <- POST(uri, headers: _*)
      response <- client.flatMap(res => res.run(request)).use(response => IO.pure(response.status))
    } yield response


  def placeBattleship(x: Int, y: Int, direction: String, gameId: String): IO[Status] =
    for {
      uri <- IO.fromEither(Uri.fromString(httpConfig.api + "/api/tournaments" + s"/$gameId" + "/battleships"))
      request  <- POST(uri, Header("Content-Type", "application/json"))
      response <- client.flatMap(res => res.run(request)).use(response => IO.pure(response.status))
    } yield response

  private def getAuthHeaders(jwt: String): IO[List[Header]] =
    IO(
      List(
        Header("Content-Type", "application/json"),
        Authorization(http4s.Credentials.Token(AuthScheme.Bearer, jwt)
        )
      )
    )

}

object HttpClient {

  implicit def credentialsEntityEncoder: EntityEncoder[IO, Credentials] =
    jsonEncoderOf[IO, Credentials]

  implicit def JWTEntityDecoder: EntityDecoder[IO, JWT] =
    jsonOf[IO, JWT]

  def make(httpConfig: HttpConfig)(implicit ec: ExecutionContext, concurrentEffect: ConcurrentEffect[IO], t: Timer[IO], logger: Logger[IO]) =
    IO {
      val resource = BlazeClientBuilder[IO](ec).resource
      new HttpClient(httpConfig, resource)
    }
}
