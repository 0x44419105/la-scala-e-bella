
case class KafkaConfig()

case class HttpConfig(credentials: Credentials, api: String, tournament: String)

case class BootstrapServers(serverSetting: String)
case class GroupId(id: String)

/**
 * Team authentication
 */
case class Credentials(username: String, password: String)

/**
 * Authentication JWT Response
 */
case class JWT(token: String)

/**
 * Receive GameStartedEvent (via Kafka)
 */
case class GameStart(
    gameId: String,
    tournamentId: String,
    battlegroundSize: Int,
    core: BattleshipComponent,
    battleshipTemplate: BattleshipTemplate
)

case class BattleshipComponent(symbol: String, hp: Int)
case class BattleshipTemplate(canvas: List[List[BattleshipComponent]], width: Int, height: Int)

/**
 * The coordinates (x, y) represent the position of the top left cell on the battlefield, when the battleshipis
 * facing in the selected direction.
 */
case class BattleshipPosition(
    gameId: String,
    x: Int,
    y: Int,
    direction: String
)

/**
 * Receive RoundStartedEvent (via Kafka)
 */
case class RoundStart(
    gameId: String,
    tournamentId: String,
    roundNo: Int
)

/**
 * Shoot (via Kafka)
 */
case class ShootEvent(
    tournamentId: String,
    gameId: String,
    roundNo: Int,
    x: Int,
    y: Int
)

object ShootEvent {

  def fromRoundStart(s: RoundStart, values: (Int,Int)) =
    ShootEvent(s.tournamentId, s.gameId, s.roundNo, values._1, values._2)
}

/**
 * Receive RoundEndedEvent (via Kafka)
 */
case class RoundEnd(
    gameId: String,
    tournamentId: String,
    roundNo: Int,
    shots: List[Shoot]
)

case class Shoot(
    x: Int,
    y: Int,
    status: String
)

/**
 * Receive GameEndedEvent (via Kafka)
 */
case class GameEnd(
    gameId: String,
    tournamentId: String
)
