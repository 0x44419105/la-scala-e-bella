import scala.annotation.tailrec
import scala.collection.concurrent.TrieMap
import scala.util.Random

case class Canvas(x: Int, y: Int) {

  val MISS = "MISS"
  val HIT = "HIT"
  val KILL = "KILL"

  private def randomNegative =
    if (Random.nextBoolean()) -1 else 1

  private def xOneStep() =
    Random.nextInt(1) * randomNegative

  private def yOneStep() =
    Random.nextInt(1) * randomNegative

  /**
   * Mark our ship on the canvas.
   * Ideas: remember the symbols in order to avoid them
   */
  def init(cs: BattleshipTemplate, battleshipPosition: BattleshipPosition): this.type = {
    for {
      kvPair <- battleshipPositionToCoordinates(cs, battleshipPosition)
    } value += kvPair

    this
  }

  def battleshipPositionToCoordinates(cs: BattleshipTemplate, bgs: BattleshipPosition) = {
    val pairs = for {
      i <- 0 to cs.width
      j <- 0 to cs.height
    } yield {
      if (bgs.direction == "NORTH") {
        if (cs.canvas(i)(j).symbol != " ") {
          (bgs.x + i, bgs.y + j) -> "V"
        } else {
          (bgs.x + i, bgs.y + j) -> "I"
        }
      }
      else if (bgs.direction == "SOUTH") {
        if (cs.canvas(i)(j).symbol != " ") {
          (bgs.x - i, bgs.y - j) -> "V"
        } else {
          (bgs.x + i, bgs.y + j) -> "I"
        }
      }
      else if (bgs.direction == "EAST") {
        if (cs.canvas(i)(j).symbol != " ") {
          (bgs.x - i, bgs.y + j) -> "V"
        } else {
          (bgs.x + i, bgs.y + j) -> "I"
        }
      }
      else if (bgs.direction == "WEST") {
        if (cs.canvas(i)(j).symbol != " ") {
          (bgs.x + i, bgs.y - j) -> "V"
        } else {
          (bgs.x + i, bgs.y - j) -> "I"
        }
      }
      else (bgs.x + i, bgs.y - j) -> "V"
    }

    pairs.filter(e => e._2 != "I")
  }

  private def next(currentX: Int, currentY: Int): (Int, Int) = {
    var newX = 0
    var newY = 0

    do {
      newX = currentX + xOneStep
      newY = currentY + yOneStep
    } while (newX <= x && newY <= y)

    newX -> newY
  }

  /*
   * Only store explored cells
   */
  private val value = TrieMap.empty[(Int, Int), String]

  /**
   * Store the shooting results
   */
  def mark(roundEnd: RoundEnd): this.type = {
    for {
      shot <- roundEnd.shots
    } value += (shot.x, shot.y) -> shot.status

    this
  }

  final def proposeShot: (Int, Int) = {
    val res0 = randomXY(x)
    if (!value.contains(res0)) res0 else proposeShot
  }

  def randomXY(battlegroundSize: Int) = {
    (Random.nextInt(battlegroundSize), Random.nextInt(battlegroundSize))
  }

  /**
   * Find a position, not our bts and not killed bts.
   * Stay within the limit of x,y.
   * Prefer a HIT with no KILLED one step distance
   * ex: (5,5), HIT with (5,6) KILLED should be avoided
   */
  def findNext(): Option[(Int, Int)] =
    value
      .find { case ((_, _), v) => v == HIT }
      .map { case ((x, y), _) => next(x, y) }

}

object Canvas {
  
  def proposeInit(gs: GameStart): BattleshipPosition = {
    val paramTuple = genRandomParameters(gs.battlegroundSize)

    if (checkIntegrity(
      paramTuple._1,
      paramTuple._2,
      paramTuple._3,
      (gs.battleshipTemplate.width, gs.battleshipTemplate.height),
      gs.battlegroundSize)
    ) {
      BattleshipPosition(gs.gameId, paramTuple._1, paramTuple._2, paramTuple._3)
    } else {
      proposeInit(gs)
    }
  }

  def genRandomParameters(bgs: Int) = {
    val randomPositionX = Random.nextInt(bgs)
    val randomPositionY = Random.nextInt(bgs)
    val randDir = Random.nextFloat()
    val randomDirection = if (randDir > 0.0 && randDir < 0.25) "NORTH"
    else if (randDir >= 0.25 && randDir < 0.5) "SOUTH"
    else if (randDir >= 0.5 && randDir < 0.75) "EAST"
    else if (randDir >= 0.75 && randDir < 1) "WEST"
    else "WEST"

    (randomPositionX, randomPositionY, randomDirection)
  }

  def checkIntegrity(x: Int, y: Int, dir: String, battleshipSize: (Int, Int), battlegroundSize: Int): Boolean = {
    if (dir == "NORTH" && (x + battleshipSize._1 > battlegroundSize || y + battleshipSize._2 > battlegroundSize)) return true
    else if (dir == "SOUTH" && ((x - battleshipSize._1 < 0 || y - battleshipSize._2 < 0))) return true
    else if (dir == "EAST" && ((x - battleshipSize._1 < 0 || y + battleshipSize._2 > battlegroundSize))) return true
    else if (dir == "WEST" && ((x + battleshipSize._1 > battlegroundSize || y - battleshipSize._2 < 0))) return true
    false
  }
}
